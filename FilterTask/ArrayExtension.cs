﻿using System;

namespace FilterTask
{
    public static class ArrayExtension
    {
        public static int[] FilterByDigit(int[] source, int digit)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Length == 0)
            {
                throw new ArgumentException(null, nameof(source));
            }

            if (digit < 0 || digit > 9)
            {
                throw new ArgumentOutOfRangeException(nameof(digit));
            }

            int count = 0;
            bool[] flags = new bool[source.Length];

            for (int i = 0; i < source.Length; i++)
            {
                int number = Math.Abs(source[i]);
                bool flag = false;

                if (number == digit)
                {
                    flag = true;
                    number = 0;
                }

                while (number > 0)
                {
                    if (number % 10 == digit)
                    {
                        flag = true;
                        break;
                    }

                    number /= 10;
                }

                if (flag)
                {
                    count++;
                    flags[i] = true;
                }
                else
                {
                    flags[i] = false;
                }
            }

            int[] result = new int[count];

            for (int i = 0, j = 0; i < source.Length; i++)
            {
                if (flags[i])
                {
                    result[j] = source[i];
                    j++;
                }
            }

            return result;
        }
    }
}
